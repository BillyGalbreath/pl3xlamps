package net.pl3x.pl3xlamps.runners;

import java.util.List;

import net.pl3x.pl3xlamps.Lamp;
import net.pl3x.pl3xlamps.Pl3xLamps;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;

public class LampTask implements Runnable {
	private Pl3xLamps plugin;

	public LampTask(Pl3xLamps plugin) {
		this.plugin = plugin;
	}

	@Override
	public void run() {
		if (plugin.locked) {
			plugin.debug("Skipping task because previous task still isnt finished.");
			return;
		}
		plugin.locked = true;
		List<Lamp> lamps = plugin.getDatabase().find(Lamp.class).findList();
		for (World world : plugin.getServer().getWorlds()) {
			for (Lamp lampChk : lamps) {
				Location loc = lampChk.getLocation();
				try {
					if (loc.getWorld() != world)
						continue;
					Lamp lamp = plugin.getLamp(loc);
					if (lamp == null)
						continue;
					Material newMat = plugin.timeToTurnOn(lamp.getName(), world.getTime()) ? Material.REDSTONE_LAMP_ON : Material.REDSTONE_LAMP_OFF;
					if (loc.getBlock().getType().equals(newMat))
						continue;
					loc.getBlock().setType(newMat);
				} catch (Exception e) {
					// ignore
				}
			}
		}
		plugin.locked = false;
	}
}
