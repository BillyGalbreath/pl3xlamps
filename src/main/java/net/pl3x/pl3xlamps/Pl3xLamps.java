package net.pl3x.pl3xlamps;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.persistence.PersistenceException;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import net.pl3x.pl3xlamps.listeners.LampListener;
import net.pl3x.pl3xlamps.listeners.PlayerListener;
import net.pl3x.pl3xlamps.runners.LampTask;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitTask;
import org.mcstats.Metrics;

public class Pl3xLamps extends JavaPlugin {
	public boolean locked = false;
	public HashMap<String, String> lampTypes = new HashMap<String, String>(); // <name,condition>
	private BukkitTask bukkitTask;

	@Override
	public List<Class<?>> getDatabaseClasses() {
		List<Class<?>> list = new ArrayList<Class<?>>();
		list.add(Lamp.class);
		return list;
	}

	public void onEnable() {
		if (!new File(getDataFolder() + File.separator + "config.yml").exists())
			saveDefaultConfig();

		try {
			getDatabase().find(Lamp.class).findRowCount();
		} catch (PersistenceException ex) {
			log("Installing database for " + getDescription().getName() + " due to first time usage");
			installDDL();
		}

		Bukkit.getPluginManager().registerEvents(new LampListener(this), this);
		Bukkit.getPluginManager().registerEvents(new PlayerListener(this), this);

		reloadLampTypes();

		if (!this.isEnabled())
			return;

		restartRunners();

		try {
			Metrics metrics = new Metrics(this);
			metrics.start();
		} catch (IOException e) {
			log("&4Failed to start Metrics: &e" + e.getMessage());
		}

		log(getName() + " v" + getDescription().getVersion() + " by BillyGalbreath enabled!");
	}

	public void onDisable() {
		if (bukkitTask != null)
			bukkitTask.cancel();
		bukkitTask = null;
		lampTypes.clear();
		lampTypes = null;
		log(getName() + " Disabled.");
	}

	public void log(Object obj) {
		if (getConfig().getBoolean("color-logs", true)) {
			getServer().getConsoleSender().sendMessage(colorize("&3[&d" + getName() + "&3]&r " + obj));
		} else {
			Bukkit.getLogger().log(Level.INFO, "[" + getName() + "] " + ((String) obj).replaceAll("(?)\u00a7([a-f0-9k-or])", ""));
		}
	}

	public void debug(Object obj) {
		if (getConfig().getBoolean("debug-mode"))
			log(obj);
	}

	public String colorize(String str) {
		return str.replaceAll("(?i)&([a-f0-9k-or])", "\u00a7$1");
	}

	public void restartRunners() {
		if (bukkitTask != null)
			bukkitTask.cancel();
		bukkitTask = getServer().getScheduler().runTaskTimer(this, new LampTask(this), 0, getConfig().getInt("check-interval", 20));
	}

	public void reloadLampTypes() {
		lampTypes.clear();
		if (!getConfig().contains("lamps.0")) {
			log("&4ERROR: No lamps have been setup in the config.yml!");
			log("&4       If you have upgraded from an earlier version please");
			log("&4       check the example config.yml as things have changed.");
			log("&4");
			log("&4Will now disable plugin to prevent data corruption.");
			Bukkit.getPluginManager().disablePlugin(this);
			return;
		}
		for (String key : getConfig().getConfigurationSection("lamps").getKeys(false)) {
			ConfigurationSection lamp = getConfig().getConfigurationSection("lamps." + key);
			String name = lamp.getString("name");
			String condition = lamp.getString("turn-on-condition");
			lampTypes.put(name, condition);
			debug("Added new LampType named " + name + " with condition '" + condition + "'");
		}
	}

	public Lamp getLamp(Location loc) {
		return getDatabase().find(Lamp.class).where().ieq("world", loc.getWorld().getName()).ieq("x", Integer.toString(loc.getBlockX())).ieq("y", Integer.toString(loc.getBlockY())).ieq("z", Integer.toString(loc.getBlockZ())).findUnique();
	}

	public boolean timeToTurnOn(String lampName, long time) {
		if (!lampTypes.containsKey(lampName))
			return false;
		String condition = lampTypes.get(lampName);
		ScriptEngineManager factory = new ScriptEngineManager();
		ScriptEngine engine = factory.getEngineByName("JavaScript");
		try {
			engine.eval("time = " + time);
			return (Boolean) engine.eval(condition);
		} catch (ScriptException e) {
			return false;
		}
	}
}
