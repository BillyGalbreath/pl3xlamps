package net.pl3x.pl3xlamps.listeners;

import java.util.List;

import net.pl3x.pl3xlamps.Lamp;
import net.pl3x.pl3xlamps.Pl3xLamps;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPistonExtendEvent;
import org.bukkit.event.block.BlockPistonRetractEvent;
import org.bukkit.event.block.BlockRedstoneEvent;

public class LampListener implements Listener {
	private Pl3xLamps plugin;

	public LampListener(Pl3xLamps plugin) {
		this.plugin = plugin;
	}

	private void moveLamp(BlockFace direction, Location origLoc) {
		Lamp lamp = plugin.getLamp(origLoc);
		if (lamp == null)
			return;
		String oldName = lamp.getName();
		double x = origLoc.getBlockX() + direction.getModX();
		double y = origLoc.getBlockY() + direction.getModY();
		double z = origLoc.getBlockZ() + direction.getModZ();
		Location newLoc = new Location(origLoc.getWorld(), x, y, z);
		plugin.getDatabase().delete(lamp);
		lamp = plugin.getLamp(newLoc);
		if (lamp == null)
			lamp = new Lamp();
		lamp.setLocation(newLoc);
		lamp.setName(oldName);
		plugin.getDatabase().save(lamp);
		plugin.debug("Lamp moved by piston: " + origLoc.getBlockX() + "," + origLoc.getBlockY() + "," + origLoc.getBlockZ() + " to " + newLoc.getBlockX() + "," + newLoc.getBlockY() + "," + newLoc.getBlockZ());
	}

	/*
	 * Keep lights on if condition is still true
	 */
	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void onBlockRedstoneEvent(BlockRedstoneEvent event) {
		Block block = event.getBlock();
		long time = block.getWorld().getTime();
		Lamp lamp = plugin.getLamp(block.getLocation());
		if (lamp == null)
			return;
		if (!plugin.timeToTurnOn(lamp.getName(), time))
			return;
		event.setNewCurrent(100);
		event.getBlock().setType(Material.REDSTONE_LAMP_ON);
		Location bLoc = block.getLocation();
		if (plugin.getConfig().getBoolean("spammy-debug-mode"))
			plugin.log("Keeping lamp Turned on: " + bLoc.getBlockX() + "," + bLoc.getBlockY() + "," + bLoc.getBlockZ());
	}

	/*
	 * Keep track of lamps when pushed by piston (prevents duplication bug)
	 */
	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void onBlockPistonExtendEvent(BlockPistonExtendEvent event) {
		List<Block> blocks = event.getBlocks();
		if (blocks.isEmpty())
			return;
		BlockFace direction = event.getDirection();
		for (int i = event.getLength() - 1; i >= 0; i--)
			// MUST iterate backwards!!
			moveLamp(direction, blocks.get(i).getLocation());
	}

	/*
	 * Keep track of lamps when pushed by piston (prevents duplication bug)
	 */
	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void onBlockPistonRetractEvent(BlockPistonRetractEvent event) {
		Block block = event.getBlock();
		if (!block.getType().equals(Material.PISTON_STICKY_BASE))
			return;
		moveLamp(event.getDirection().getOppositeFace(), event.getRetractLocation());
	}
}
