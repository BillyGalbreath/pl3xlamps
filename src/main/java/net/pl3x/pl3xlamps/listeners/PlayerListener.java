package net.pl3x.pl3xlamps.listeners;

import net.pl3x.pl3xlamps.Lamp;
import net.pl3x.pl3xlamps.Pl3xLamps;

import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.AnvilInventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.Repairable;
import org.bukkit.scheduler.BukkitRunnable;

public class PlayerListener implements Listener {
	private Pl3xLamps plugin;

	public PlayerListener(Pl3xLamps plugin) {
		this.plugin = plugin;
	}

	private boolean isNamed(ItemMeta meta) {
		if (meta == null)
			return false;
		if (meta.getDisplayName() == null)
			return false;
		for (String name : plugin.lampTypes.keySet()) {
			if (meta.getDisplayName().equals(name))
				return true;
		}
		return false;
	}

	/*
	 * Saves lamp data when placed
	 */
	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void onBlockPlaceEvent(BlockPlaceEvent event) {
		ItemStack item = event.getPlayer().getItemInHand();
		if (!item.getType().equals(Material.REDSTONE_LAMP_ON) && !item.getType().equals(Material.REDSTONE_LAMP_OFF))
			return;
		if (!isNamed(item.getItemMeta()))
			return;
		Player p = event.getPlayer();
		if (!p.hasPermission("pl3xlamps.place")) {
			p.sendMessage(plugin.colorize("&4You do not have permission to use this item!"));
			event.setCancelled(true);
			return;
		}
		Location loc = event.getBlock().getLocation();
		Lamp lamp = plugin.getLamp(loc);
		if (lamp == null)
			lamp = new Lamp();
		lamp.setLocation(loc);
		lamp.setName(item.getItemMeta().getDisplayName());
		plugin.getDatabase().save(lamp);

		plugin.debug(p.getName() + " placed Lamp: " + loc.getBlockX() + "," + loc.getBlockY() + "," + loc.getBlockZ());
	}

	/*
	 * Restores name data when broke
	 */
	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void onBlockBreakEvent(BlockBreakEvent event) {
		Location loc = event.getBlock().getLocation();
		Lamp lamp = plugin.getLamp(loc);
		if (lamp == null)
			return;
		Player p = event.getPlayer();
		if (!p.hasPermission("pl3xlamps.break")) {
			p.sendMessage(plugin.colorize("&4You do not have permission to break this item!"));
			event.setCancelled(true);
			return;
		}
		String oldName = lamp.getName();
		plugin.getDatabase().delete(lamp);
		plugin.debug(p.getName() + " broke Lamp: " + loc.getBlockX() + "," + loc.getBlockY() + "," + loc.getBlockZ());
		if (event.getPlayer().getGameMode().equals(GameMode.CREATIVE))
			return;
		ItemStack item = new ItemStack(Material.REDSTONE_LAMP_OFF, 1);
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(oldName);
		if (meta instanceof Repairable)
			((Repairable) meta).setRepairCost(0);
		item.setItemMeta(meta);
		loc.getWorld().dropItemNaturally(loc, item);
		event.getBlock().setType(Material.AIR);
	}

	/*
	 * Fix stacking issue when player renames
	 */
	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void onInventoryClick(InventoryClickEvent event) {
		if (event.isCancelled())
			return;
		if (!(event.getInventory() instanceof AnvilInventory))
			return;
		HumanEntity ent = event.getWhoClicked();
		if (!(ent instanceof Player))
			return;
		final Player p = (Player) ent;
		int rawSlot = event.getRawSlot();
		if (rawSlot != event.getView().convertSlot(rawSlot))
			return;
		if (rawSlot != 2)
			return;
		ItemStack item = event.getCurrentItem();
		if (item == null)
			return;
		if (!item.getType().equals(Material.REDSTONE_LAMP_OFF))
			return;
		if (!isNamed(item.getItemMeta())) {
			plugin.debug(" wrong name!");
			return;
		}
		if (!p.hasPermission("pl3xlamps.create")) {
			p.sendMessage(plugin.colorize("&4You do not have permission to create this item!"));
			event.setCancelled(true);
			return;
		}
		ItemMeta meta = item.getItemMeta();
		if (meta instanceof Repairable)
			((Repairable) meta).setRepairCost(0);
		item.setItemMeta(meta);
		event.setCurrentItem(item);
		plugin.getServer().getScheduler().runTaskLater(plugin, new BukkitRunnable() {
			@SuppressWarnings("deprecation")
			@Override
			public void run() {
				p.updateInventory();
			}
		}, 0);
	}
}
